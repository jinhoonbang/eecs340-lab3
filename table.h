#ifndef _table
#define _table


#include <iostream>
#include "node.h"
#include "link.h"

using namespace std;

#if defined(GENERIC)
class Table {
  // Students should write this class

 public:
  ostream & Print(ostream &os) const;
};
#endif


#if defined(LINKSTATE)
class Table {
  // Students should write this class
 public:
  ostream & Print(ostream &os) const;
};
#endif

#if defined(DISTANCEVECTOR)

#include <deque>


class Node;

struct Row{
  unsigned dest;
  unsigned hop;
  double lat;
};

class Table {
 private:
  deque<Row> table;

 public:
  Table();
  Table(Node n);

  double readLatency(unsigned dest);
  double readHop(unsigned dest);
  void writeRow(unsigned dest, unsigned hop, unsigned lat);

  ostream & Print(ostream &os) const;
};
#endif

inline ostream & operator<<(ostream &os, const Table &t) { return t.Print(os);}

#endif
