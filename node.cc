#include "node.h"
#include "context.h"
#include "error.h"


Node::Node(const unsigned n, SimulationContext *c, double b, double l) :
    number(n), context(c), bw(b), lat(l)
{}

Node::Node()
{ throw GeneralException(); }

Node::Node(const Node &rhs) :
  number(rhs.number), context(rhs.context), bw(rhs.bw), lat(rhs.lat) {}

Node & Node::operator=(const Node &rhs)
{
  return *(new(this)Node(rhs));
}

void Node::SetNumber(const unsigned n)
{ number=n;}

unsigned Node::GetNumber() const
{ return number;}

void Node::SetLatency(const double l)
{ lat=l;}

double Node::GetLatency() const
{ return lat;}

void Node::SetBW(const double b)
{ bw=b;}

double Node::GetBW() const
{ return bw;}

Node::~Node()
{}

deque<Node*> *Node::GetNeighbors() {
  return context->GetNeighbors(this);
}

deque<Link*> *Node::GetOutgoingLinks() {
  return context->GetOutgoingLinks(this);
}

// Implement these functions  to post an event to the event queue in the event simulator
// so that the corresponding node can recieve the ROUTING_MESSAGE_ARRIVAL event at the proper time
void Node::SendToNeighbors(const RoutingMessage *m)
{
  deque<Node*> *neighbors = this->GetNeighbors();
  for (deque<Node*>::iterator i = neighbors->begin(); i!=neighbors->end(); ++i){
    RoutingMessage *message = new RoutingMessage(*m);
    SendToNeighbor(*i, message);
  }
}

void Node::SendToNeighbor(const Node *n, const RoutingMessage *m)
{
  Link *link = new Link();
  link->SetSrc(this->GetNumber());
  link->SetDest(n->GetNumber());

  Link *neighbor = context->FindMatchingLink(link);
  if (neighbor != 0){
    Event *e = new Event(context->GetTime() + neighbor->GetLatency(), ROUTING_MESSAGE_ARRIVAL, n, m);
    context->PostEvent(e);
  }
}

void Node::SetTimeOut(const double timefromnow)
{
  context->TimeOut(this,timefromnow);
}


bool Node::Matches(const Node &rhs) const
{
  return number==rhs.number;
}


#if defined(GENERIC)
void Node::LinkHasBeenUpdated(const Link *l)
{
  cerr << *this << " got a link update: "<<*l<<endl;
  //Do Something generic:

  SendToNeighbors(new RoutingMessage);
}


void Node::ProcessIncomingRoutingMessage(const RoutingMessage *m)
{
  cerr << *this << " got a routing messagee: "<<*m<<" Ignored "<<endl;
}


void Node::TimeOut()
{
  cerr << *this << " got a timeout: ignored"<<endl;
}

Node *Node::GetNextHop(const Node *destination) const
{
  return 0;
}

Table *Node::GetRoutingTable() const
{
  return new Table;
}


ostream & Node::Print(ostream &os) const
{
  os << "Node(number="<<number<<", lat="<<lat<<", bw="<<bw<<")";
  return os;
}

#endif

#if defined(LINKSTATE)


void Node::LinkHasBeenUpdated(const Link *l)
{
  cerr << *this<<": Link Update: "<<*l<<endl;
}


void Node::ProcessIncomingRoutingMessage(const RoutingMessage *m)
{
  cerr << *this << " Routing Message: "<<*m;
}

void Node::TimeOut()
{
  cerr << *this << " got a timeout: ignored"<<endl;
}

Node *Node::GetNextHop(const Node *destination) const
{
  // WRITE
  return 0;
}

Table *Node::GetRoutingTable() const
{
  // WRITE
  return 0;
}


ostream & Node::Print(ostream &os) const
{
  os << "Node(number="<<number<<", lat="<<lat<<", bw="<<bw<<")";
  return os;
}
#endif


#if defined(DISTANCEVECTOR)

void Node::LinkHasBeenUpdated(const Link *l)
{
  cerr << *this<<": Link Update: "<<*l<<endl;

  for (deque<Row>::iterator i = this->table->begin(); i != this->table->end() ; ++i){
    //i is a neighbor
    if (i->dest == l->GetDest()){
      this->table->writeRow(l->GetDest(), l->GetDest(),l->GetLatency());
    }
  }

  //find neighbor to whom the latency has changed
  deque<Node*> *neighbors = this->GetNeighbors();
  Node *neighbor;
  for (deque<Node*>::iterator j = neighbors->begin(); j!= neighbors->end(); ++j){
    if (l->GetDest() == (**j).dest){
      neighbor = *j;
    }
  }
  delete neighbors;

  //for each dest, see if new latency is less than the existing latency
  for (deque<Row>::iterator i = this.table->begin(); i != this->table->end(); ++i){
    if (i->dest != neighbor->GetNumber()){

      double lat_new = l->GetLatency() + neighbor->table->readLatency(i->dest);

      if (lat_new < i->lat){
        this->table->writeRow(i->dest, neighbor->GetNumber(),lat_new);
      }
    }
  }

  //send routing message
  RoutingMessage *m = new RoutingMessage(this->GetNumber(), l->GetDest(), l->GetLatency);
  SendToNeighbors(m);
}
  // // update our table
  // this.table->writeDist(l->GetNumber(), l->GetDest(), l->GetLatency());
  //   // send out routing mesages

  // RoutingMessage *m = new RoutingMessage(this->GetNumber(), l->GetDest(), l->GetLatency);

  // deque<Node*> *neighbors = this->GetNeighbors();
  // deque<Link*> *links = this->GetOutgoingLinks();

  // Node *neighbor;

  // for (deque<Link*>::iterator i = links->begin(); i != links->end(); ++i) {
  //   for (deque<Node*>::iterator j = neighbors->begin(); j != nodes->end(); ++j) {
  //       if (Node((*i)->GetDest(), 0, 0, 0).Matches(**j)) {
  //         neighbor = *j;
  //         break;
  //       }
  //     }
  //     Event *e = new Event(context->GetTime() + (*i)->GetLatency(), ROUTING_MESSAGE_ARRIVAL, neighbor, m);
  //     context->PostEvent(e);
  //   }

void Node::ProcessIncomingRoutingMessage(const RoutingMessage *m)
{
  cout<<"RoutingMessage"<<endl;

  bool notFound = true;

  for (deque<Row>::iterator i = this->table->begin(); i i= this->table->end(); ++i){
    //i is a neighbor
    if (i->dest == m->dest){
      notFound = false;
      if (m->lat+this->table->readLatency(m->src) < i->lat){
        this->table->writeRow(i->dest, this->table->readHop(m->src), m->lat+this->table->readLatency(m->src));

        RoutingMessage *m_new = new RoutingMessage(this->GetNumber(), i->dest, m->lat+this->table->readLatency(m->src));

        SendToNeighbors(m_new)
      }
    }
  }
}

void Node::TimeOut()
{
  cerr << *this << " got a timeout: ignored"<<endl;
}


Node *Node::GetNextHop(const Node *destination) const
{
  unsigned dest = destination->GetNumber();
  unsigned hop = this.table->readHop(dest);

  deque<Node*> *neighbors = this->GetNeighbors();

  for (deque<Node*>::iterator i = neighbors->begin(); i!= neighbors->end(); ++i){
    if ((**i).GetNumber() == hop){
      return new Node(**i);
    }
  }
  // unsigned dest = destination->GetNumber();
  // unsigned nxt_node = his.table->readRoute(dest);

  // deque<Node*> *nodes = this->GetNeighbors();

  // for (deque<Node*>::const_iterator i = nodes->begin(); i != nodes->end(); ++i) {
  //     if ((Node(nxt_node, 0, 0, 0).Matches(**i))) {
  //         return new Node(**i);
  //     }
  // }
  // return 0;
}

Table *Node::GetRoutingTable() const
{
  return new Table(this.table);
}


ostream & Node::Print(ostream &os) const
{
  os << "Node(number="<<number<<", lat="<<lat<<", bw="<<bw;
  return os;
}
#endif
