#include "messages.h"


#if defined(GENERIC)
ostream &RoutingMessage::Print(ostream &os) const
{
  os << "RoutingMessage()";
  return os;
}
#endif


#if defined(LINKSTATE)

ostream &RoutingMessage::Print(ostream &os) const
{
  return os;
}

RoutingMessage::RoutingMessage()
{}


RoutingMessage::RoutingMessage(const RoutingMessage &rhs)
{}

#endif


#if defined(DISTANCEVECTOR)

ostream &RoutingMessage::Print(ostream &os) const
{
  return os << "src:" << this.src
            << "dest:" << this.dest
            << "lat:" << this.lat << endl;
}

RoutingMessage::RoutingMessage()
{}
RoutingMessage::RoutingMessage(unsigned src, unsigned dest, unsigned lat){
  this.src = src;
  this.dest = dest;
  this.lat = lat;
}

RoutingMessage::RoutingMessage(const RoutingMessage &rhs)
{
  this.src = rhs.src;
  this.dest; = rhs.dest;
  this.lat; = rhs.lat;
}

#endif

