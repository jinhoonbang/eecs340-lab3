#include "table.h"

#if defined(GENERIC)
ostream & Table::Print(ostream &os) const
{
  // WRITE THIS
  os << "Table()";
  return os;
}
#endif

#if defined(LINKSTATE)

#endif

#if defined(DISTANCEVECTOR)

//gets next hop
// unsigned Table::readRoute(unsigned dest){
//   return t_route[dest];
// }

// void Table::writeRoute(unsigned dest, unsigned nxt){
//   t_route[dest] = nxt;
// }

// unsigned Table::readDist(unsigned row, unsigned dest){
//   return t_dist[row][dest];
// }

// void Table::writeDist(unsigned row, unsigned dest, doube lat){
//   t_dist[row][dest] = lat;
// }

// unsigned Table::writeDistRow(unsigned dest, ){

// }

double Table::readLatency(unsigned dest){
  for (deque<Row>::iterator i = this.table->begin(); i != this.table->end() ; ++i){
    if (i->dest == dest){
      return i->lat;
    }
  }
  return 99999;
}

double Table::readHop(unsigned dest){
  for (deque<Row>::iterator i = this.table->begin(); i != this.table->end(); ++i){
    if (i->dest == dest){
      return i->hop;
    }
  }
  return -1;
}

void Table::writeRow(unsigned dest, unsigned hop, unsigned lat){
  for (deque<Row>::iterator i = this.table->begin(); i != this.table->end() ; ++i){
    if (i->dest == dest){
      i->hop = hop;
      i->lat = lat;
    }
  }
  this.table.push_back(*new Row(dest, hop, lat));
}




#endif
